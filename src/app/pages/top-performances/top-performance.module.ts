import {NgModule} from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {SharedModule} from '../../shared/shared.module';
import {FormsModule, ReactiveFormsModule } from '@angular/forms';
import {MatTableModule} from '@angular/material';
import {TopPerformanceComponent} from './top-performance.component';

const topPerformanceRoutes: Routes =[
    {
        path:'',
        component: TopPerformanceComponent
    }
];
@NgModule({
    declarations:[
        TopPerformanceComponent
    ],
    imports:[
        SharedModule,
        FormsModule, 
        ReactiveFormsModule,
        MatTableModule,
        RouterModule.forChild(topPerformanceRoutes)
    ],
})
export class TopPerformanceModule{}