import {
  Component,
  OnInit,
  ViewChild,
  ViewEncapsulation,
  OnChanges
} from "@angular/core";
import { BrandingRequestService } from "../../services/branding-request.service";
import { MatSort, MatTableDataSource, MatPaginator } from "@angular/material";

@Component({
  selector: "top-performances",
  templateUrl: "top-performance.component.html",
  styleUrls: ["top-performance.component.css"],
  encapsulation: ViewEncapsulation.None
})
export class TopPerformanceComponent implements OnInit, OnChanges {
  states: string[] = ["East", "West", "North", "South", "All"];
  selected: string = "East";

  displayedColumns: string[] = [
    "id",
    "selling_price",
    "unit_count",
    "zonal_info",
    "timestamp"
  ];
  dataSource = new MatTableDataSource();
  itemKeys: any;

  constructor(private brandService: BrandingRequestService) {}
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  ngOnInit() {
    this.brandService.getData(this.selected).subscribe(data => {
      this.dataSource.data = data;
    });
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  onSelected() {
    this.brandService.getData(this.selected).subscribe(data => {
      this.dataSource.data = data;
    });
  }

  ngOnChanges() {}

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}