import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { EventService } from "../../services/event.service";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";

@Component({
  selector: "app-earnings",
  templateUrl: "./earnings.component.html",
  styleUrls: ["./earnings.component.css"]
})
export class EarningsComponent implements OnInit {
  earningForm: FormGroup;
  isValid: boolean;
  private formSubmitAttempt: boolean;
  states: string[] = [
    "January",
    "Febuary",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December"
  ];

  constructor(
    private eventService: EventService,
    private fb: FormBuilder,
    private router: Router
  ) {
    this.earningForm = this.fb.group({
      cp_phone: ["", Validators.required],
      month: ["", Validators.required],
      sellingPrice: ["", Validators.required],
      commission: ["", Validators.required]
    });
  }

  ngOnInit() {}

  isFieldInvalid(field: string) {
    return (
      (!this.earningForm.get(field).valid &&
        this.earningForm.get(field).touched) ||
      (this.earningForm.get(field).untouched && this.formSubmitAttempt)
    );
  }
  onEventCreationSubmit() {
    if (this.earningForm.valid) {
      console.log("commission", "commission submitted");
    }
    this.formSubmitAttempt = true;
  }
}
