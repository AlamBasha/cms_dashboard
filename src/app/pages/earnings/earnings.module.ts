import {NgModule} from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {SharedModule} from '../../shared/shared.module';
import {MatSelectModule} from '@angular/material/select';
import {FormsModule, ReactiveFormsModule } from '@angular/forms';
import {EarningsComponent} from './earnings.component';

const earningRoutes: Routes = [
  {
    path: '',
    component: EarningsComponent
  }
];

@NgModule({
  declarations: [
    EarningsComponent
  ],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    MatSelectModule,
    RouterModule.forChild(earningRoutes)
  ],
})
export class EarningsModule { }
