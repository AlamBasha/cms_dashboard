import {NgModule} from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {SharedModule} from '../../shared/shared.module';
import {FormsModule, ReactiveFormsModule } from '@angular/forms';
import {TabsComponent} from './tabs.component';
import {BasicInfoComponent} from './basic-info/basic-info.component';
import {AmenitiesComponent} from './amenities/amenities.component';
import {LocationComponent} from './location/location.component';
import {FloorPlansComponent} from './floor-plans/floor-plans.component';
import {SpecificationsComponent} from './specifications/specifications.component';
import {ProgressComponent} from './progress/progress.component';
import {ViewPropertiesComponent} from './view-properties/view-properties.component';

const eventCreationRoutes: Routes = [
  {
    path: '',
    component: ViewPropertiesComponent
  },
  {
    path: 'updateProperty',
    component: TabsComponent
  }
];

@NgModule({
  declarations: [
    BasicInfoComponent,
    AmenitiesComponent,
    LocationComponent,
    FloorPlansComponent,
    SpecificationsComponent,
    ProgressComponent,
    ViewPropertiesComponent,
    TabsComponent
  ],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    RouterModule.forChild(eventCreationRoutes)
  ],
})
export class TabsModule { }
