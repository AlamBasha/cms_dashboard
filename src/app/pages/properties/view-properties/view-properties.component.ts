import { Component, OnInit, OnChanges, ViewChild } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router, ActivatedRoute } from "@angular/router";
import { MatSort, MatTableDataSource, MatPaginator } from "@angular/material";
import { PropertyService } from "../../../services/property.service";
import { CityService } from "../../../services/city.service";
@Component({
  selector: "app-view-properties",
  templateUrl: "view-properties.component.html",
  styleUrls: ["view-properties.component.css"]
})
export class ViewPropertiesComponent implements OnInit, OnChanges {
  cityDrop: FormGroup;
  cities: string[];
  displayedColumns: string[] = [
    "id",
    "property_name",
    "location",
    "price",
    "status"
  ];
  dataSource = new MatTableDataSource();
  itemKeys: any;
  props: any;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private propertyService: PropertyService,
    private cityService: CityService
  ) {
    this.cityDrop = this.fb.group({
      city: ["", Validators.required]
    });
  }
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  ngOnInit() {
    this.cityService.getCities().subscribe(data => {
      this.cities = data;
    });
    this.propertyService.getData({ city: "Ahmedabad" }).subscribe(data => {
      this.props = data;
      this.dataSource.data = data;
    });
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  ngOnChanges() {
    console.log("cityDrop1", this.cityDrop.get("city").value);
  }
  citySelectionSubmit() {
    let data = { city: this.cityDrop.get("city").value };
    this.propertyService.getData(data).subscribe(data => {
      this.dataSource.data = data;
    });
  }
  navgateTo(property) {
    localStorage.setItem("property", JSON.stringify(property));
  }
}
