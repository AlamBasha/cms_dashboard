import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router, ActivatedRoute } from "@angular/router";
@Component({
  selector: "app-tabs-properties",
  templateUrl: "tabs.component.html",
  styleUrls: ["tabs.component.css"]
})
export class TabsComponent implements OnInit {
  eventCreationForm: FormGroup;
  isValid: boolean;
  private formSubmitAttempt: boolean;
  property: any;

  constructor(private fb: FormBuilder, private router: Router) {
    this.eventCreationForm = this.fb.group({
      eventName: ["", Validators.required],
      description: ["", Validators.required],
      location: ["", Validators.required]
    });
  }

  ngOnInit() {
    this.property = JSON.parse(localStorage.getItem("property"));
    localStorage.removeItem("property");
  }

  isFieldInvalid(field: string) {
    return (
      (!this.eventCreationForm.get(field).valid &&
        this.eventCreationForm.get(field).touched) ||
      (this.eventCreationForm.get(field).untouched && this.formSubmitAttempt)
    );
  }
  onEventCreationSubmit() {
    // this.formSubmitAttempt = true;
  }
}
