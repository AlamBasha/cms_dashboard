import { Component, OnInit, Input } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router, ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-basic",
  templateUrl: "basic-info.component.html",
  styleUrls: ["basic-info.component.css"]
})
export class BasicInfoComponent implements OnInit {
  @Input() property: any;
  eventCreationForm: FormGroup;
  isValid: boolean;
  private formSubmitAttempt: boolean;
  property_name: string;
  location: string;
  city: string;
  price: string;
  constructor(private fb: FormBuilder, private router: Router) {
    this.eventCreationForm = this.fb.group({
      propertyName: ["", Validators.required],
      address: ["", Validators.required],
      city: ["", Validators.required],
      status: ["", Validators.required],
      price: ["", Validators.required],
      overView: ["", Validators.required]
    });
  }

  ngOnInit() {
    if (this.property !== null) {
      this.eventCreationForm.setValue({
        propertyName: this.property.property_name,
        address: this.property.location,
        city: this.property.city,
        status: this.property.status,
        price: this.property.price,
        overView: this.property.overview
      });
    }
  }

  isFieldInvalid(field: string) {
    return (
      (!this.eventCreationForm.get(field).valid &&
        this.eventCreationForm.get(field).touched) ||
      (this.eventCreationForm.get(field).untouched && this.formSubmitAttempt)
    );
  }
  onEventCreationSubmit() {}
}
