import { COMMA, ENTER } from "@angular/cdk/keycodes";
import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-specifications",
  templateUrl: "specifications.component.html",
  styleUrls: ["specifications.component.css"]
})
export class SpecificationsComponent implements OnInit {
  fields: any;
  isVisible: boolean;
  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = true;
  pre = 0;
  readonly separatorKeysCodes: number[] = [ENTER, COMMA];

  ngOnInit() {
    this.isVisible = false;
    this.fields = [
      {
        title: "",
        value: ""
      }
    ];
  }

  addNewOne() {
    this.fields.push({
      title: "",
      value: ""
    });
  }

  removeOne(field) {
    this.fields.splice(field, 1);
  }

  printAll() {
    this.isVisible = !this.isVisible;
  }

  onTitle(event, val) {
    const inputValue = event.target.value;
    for (let i = 0; i < this.fields.length; i++) {
      if (i == val) {
        this.fields[i].title = inputValue;
      }
    }
  }

  onValue(event, val) {
    const inputValue = event.target.value;
    for (let i = 0; i < this.fields.length; i++) {
      if (i == val) {
        this.fields[i].value = inputValue;
      }
    }
  }
}
