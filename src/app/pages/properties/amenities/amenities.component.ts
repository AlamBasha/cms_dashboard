import { COMMA, ENTER } from "@angular/cdk/keycodes";
import { Component, OnInit } from "@angular/core";
import { MatChipInputEvent } from "@angular/material";

@Component({
  selector: "app-amenities",
  templateUrl: "amenities.component.html",
  styleUrls: ["amenities.component.css"]
})
export class AmenitiesComponent implements OnInit {
  fields: any;
  isVisible: boolean;
  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = true;
  pre = 0;
  readonly separatorKeysCodes: number[] = [ENTER, COMMA];
  amenities: string[] = [];

  ngOnInit() {
    this.isVisible = false;
    this.fields = [
      {
        title: "",
        values: []
      }
    ];
  }

  addNewOne() {
    this.amenities = [];
    this.fields.push({
      title: "",
      values: []
    });
  }

  removeOne(field) {
    this.fields.splice(field, 1);
  }
  printAll() {
    this.isVisible = !this.isVisible;
  }
  onKey(event, val) {
    const inputValue = event.target.value;
    for (let i = 0; i < this.fields.length; i++) {
      if (i == val) {
        this.fields[i].title = inputValue;
      }
    }
  }

  add(event: MatChipInputEvent, val): void {
    const input = event.input;
    const value = event.value;
    // Add our amenity
    if ((value || "").trim()) {
      //this.amenities.push(value.trim());
      for (let i = 0; i < this.fields.length; i++) {
        if (i == val) {
          this.fields[i].values.push(value.trim());
        }
      }
    }
    // Reset the input value
    if (input) {
      input.value = "";
    }
  }

  remove(amenity: string, row, col): void {
    console.log(amenity + " " + row);
    for (let i = 0; i < this.fields.length; i++) {
      if (i == row) {
        // const index = this.fields[i].values.indexOf(amenity);
        // if (index >= 0) {
        this.fields[i].values.splice(col, 1);
      }
    }
  }
}
