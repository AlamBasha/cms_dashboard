import { COMMA, ENTER } from "@angular/cdk/keycodes";
import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-floor-plans",
  templateUrl: "floor-plans.component.html",
  styleUrls: ["floor-plans.component.css"]
})
export class FloorPlansComponent implements OnInit {
  fields: any;
  isVisible: boolean;

  ngOnInit() {
    this.isVisible = false;
    this.fields = ["alam"];
  }

  addNewOne() {
    this.fields.push("");
  }

  removeOne(field) {
    this.fields.splice(field, 1);
  }

  printAll() {
    this.isVisible = !this.isVisible;
  }
}
