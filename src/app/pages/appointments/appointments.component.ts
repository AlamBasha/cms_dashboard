import {Component, OnInit, ViewChild} from '@angular/core';
import {MatSort, MatTableDataSource,MatPaginator} from '@angular/material';
import {AppointmentService} from '../../services/appointments.service'; 

@Component({
  selector: 'appointments-table',
  styleUrls: ['appointments.component.css'],
  templateUrl: 'appointments.component.html',
})
export class AppointmentsComponent implements OnInit {
  displayedColumns: string[] = ['position', 'name', 'weight', 'symbol'];
  dataSource = new MatTableDataSource();
  constructor(private appointmentService:AppointmentService){}
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  ngOnInit() {
    this.appointmentService.getData().subscribe(
      data => {
        this.dataSource.data = data;
      }
    );
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}
