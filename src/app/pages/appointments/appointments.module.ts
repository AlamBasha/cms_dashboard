import {NgModule} from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {SharedModule} from '../../shared/shared.module';
import {FormsModule, ReactiveFormsModule } from '@angular/forms';
import {MatTableModule} from '@angular/material';
import {AppointmentsComponent} from './appointments.component';

const appointmentsTableRoutes: Routes =[
    {
        path:'',
        component: AppointmentsComponent
    }
];
@NgModule({
    declarations:[
        AppointmentsComponent
    ],
    imports:[
        SharedModule,
        FormsModule, 
        ReactiveFormsModule,
        MatTableModule,
        RouterModule.forChild(appointmentsTableRoutes)
    ],
})
export class AppointmentsModule{}