import {NgModule} from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {SharedModule} from '../../shared/shared.module';
import {FormsModule, ReactiveFormsModule } from '@angular/forms';

import {ManageLeadsComponent} from './manage-leads.component';

const manageLeadsRoutes: Routes = [
  {
    path: '',
    component: ManageLeadsComponent
  },
];

@NgModule({
  declarations: [
    ManageLeadsComponent
  ],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    RouterModule.forChild(manageLeadsRoutes)
  ],
})
export class ManageLeadsModule { }