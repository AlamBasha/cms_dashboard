import { Component, OnInit, OnChanges, ViewChild } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router, ActivatedRoute } from "@angular/router";
import { MatSort, MatTableDataSource, MatPaginator } from "@angular/material";
import { ManageLeadsService } from "../../services/manage-leads.service";
@Component({
  selector: "app-manage-leads",
  templateUrl: "manage-leads.component.html",
  styleUrls: ["manage-leads.component.css"]
})
export class ManageLeadsComponent implements OnInit, OnChanges {
  cityDrop: FormGroup;
  displayedColumns: string[] = [
    "id",
    "lead_name",
    "lead_mobile",
    "cp_name",
    "cp_mobile"
  ];
  dataSource = new MatTableDataSource();
  itemKeys: any;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private manageleadService: ManageLeadsService
  ) {}
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  ngOnInit() {
    // this.manageleadService.getData().subscribe(data => {
    //   console.log(data);
    //   this.dataSource.data = data;
    // });
    this.dataSource.data = this.manageleadService.getData();
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  ngOnChanges() {}
}
