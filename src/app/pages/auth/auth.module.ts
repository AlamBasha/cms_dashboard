import {NgModule} from '@angular/core';
import {SharedModule} from '../../shared/shared.module';
import {FormsModule, ReactiveFormsModule } from '@angular/forms';
import {LoginComponent} from './login/login.component';
import {RegComponent} from './registration/reg.component';
import {AuthRoutingModule} from './auth.routing.module';

@NgModule({
  declarations: [
    LoginComponent,
    RegComponent
  ],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    AuthRoutingModule
  ],
})
export class AuthModule { }
