import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../../../services/auth.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  formGroup: FormGroup;
  isValid: boolean;
  private formSubmitAttempt: boolean;

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private router: Router
  ) {
    this.formGroup = this.fb.group({
      userName: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  ngOnInit() {
    if (localStorage.getItem('currentUser')) {
      this.router.navigate(['dashboard']);
    }
  }

  isFieldInvalid(field: string) {
    return (
      (!this.formGroup.get(field).valid && this.formGroup.get(field).touched) ||
      (this.formGroup.get(field).untouched && this.formSubmitAttempt)
    );
  }

  onSubmit() {
    if (this.formGroup.valid) {
      this.authService.login(this.formGroup.get('userName').value, this.formGroup.get('password').value)
        .subscribe(
          data => {
            // console.log(typeof(data),data);
            if (data.valid_auth === true) {
              this.router.navigate(['/dashboard']);
            }
          }
        );
    }
    this.formSubmitAttempt = true;
  }

}
