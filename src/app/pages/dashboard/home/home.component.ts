import { Component,OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {WidgetService} from '../../../services/widget.service'
//import { count } from 'rxjs/operators';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  title = 'app';
  apiData:any;
  dateNow = Date.now();
  wind:number;
  compass:any;
  pressure:any;
  city:any;
  temp1:number;
  humidity:any;
  min_temp:number;
  max_temp:number;
  constructor(private router:Router,private widgetService:WidgetService){}
    ngOnInit(){
      this.widgetService.getData().subscribe(data => {
        this.apiData = data;
       // console.log(this.apiData);
        this.temp1=this.apiData.main['temp']-273.15;
        this.city=this.apiData.name;
        this.wind=Math.round(this.apiData.wind['speed']*2.2366);
        this.compass =this.apiData.wind['deg'];
        this.pressure=this.apiData.main['pressure'];
        this.humidity=this.apiData.main['humidity'];
        this.min_temp=this.apiData.main['temp_min']-273.15;
        this.max_temp=this.apiData.main['temp_max']-273.15;
        console.log(this.wind);
    })
}
}
  