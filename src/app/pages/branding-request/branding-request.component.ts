import {Component, OnInit,ViewChild} from '@angular/core';
import {BrandingRequestService} from '../../services/branding-request.service';
import {MatSort, MatTableDataSource,MatPaginator} from '@angular/material';

@Component({
  selector: 'branding-request',
  templateUrl: 'branding-request.component.html',
  styleUrls: ['branding-request.component.css'],
})
export class BrandingRequestComponent implements OnInit {
  states: string[] = [
    'East','West','North','South','All'
  ];

  displayedColumns: string[] = ['id', 'selling_price', 'unit_count', 'zonal_info','timestamp'];
  dataSource= new MatTableDataSource();
  itemKeys:any;

  constructor(private brandService:BrandingRequestService){}
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  ngOnInit(){
      this.brandService.getData("All").subscribe(
        data => {
         console.log(data);
          this.dataSource.data=data;
          //console.log(this.items);
        }
      );
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}