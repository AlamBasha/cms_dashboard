import {NgModule} from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {SharedModule} from '../../shared/shared.module';
import {FormsModule, ReactiveFormsModule } from '@angular/forms';
import {MatTableModule} from '@angular/material';
import {BrandingRequestComponent} from './branding-request.component';

const brandingRequestRoutes: Routes =[
    {
        path:'',
        component: BrandingRequestComponent
    }
];
@NgModule({
    declarations:[
        BrandingRequestComponent
    ],
    imports:[
        SharedModule,
        FormsModule, 
        ReactiveFormsModule,
        MatTableModule,
        RouterModule.forChild(brandingRequestRoutes)
    ],
})
export class BrandingRequestModule{}