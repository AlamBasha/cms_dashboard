export class Mail {
  id: string;
  cp_name: string;
  cp_mobile: string;
  subject: string;
  message: string;
  time: string;
  read: string;

  constructor(mail) {
    this.id = mail.id;
    this.cp_name = mail.cp_name;
    this.cp_mobile = mail.cp_mobile;
    this.subject = mail.subject;
    this.message = mail.message;
    this.time = mail.time;
    this.read = mail.read;
  }
  markRead() {
    this.read = "true";
    return;
  }

  markUnRead() {
    this.read = "false";
  }
}
