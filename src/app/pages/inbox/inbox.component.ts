import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
  OnChanges
} from "@angular/core";
import { Mail } from "./mail.model";
import { MailService } from "../../services/mail.service";

@Component({
  selector: "app-inbox",
  templateUrl: "./inbox.component.html",
  styleUrls: ["./inbox.component.css"]
})
export class InboxComponent implements OnInit, OnChanges {
  mails: Mail[];
  currentMail: Mail;
  hasSelectedMails: boolean;
  data: any;

  constructor(private mailService: MailService, private cd: ChangeDetectorRef) {
    this.mails = [];
    this.data = [];
  }

  ngOnInit() {
    this.data = this.mailService.getMails();
    this.filterMails("All");
  }

  ngOnChanges() {
    console.log("onChanges", "im in ngOnChanges");
  }

  filterMails(tag) {
    switch (tag) {
      case "All":
        this.mails = this.data;
        break;
      case "Read":
        this.mails = this.getMails("true");
        break;
      case "Unread":
        this.mails = this.getMails("false");
        break;
    }
  }

  getMails(tag) {
    let temp: Mail[] = [];
    for (let i = 0; i < this.data.length; i++) {
      if (this.data[i].read == tag) {
        temp.push(this.data[i]);
      }
    }
    return temp;
  }

  getCurrentMail(val) {
    this.currentMail = val.mail;
    this.mails = val.mails;
  }
}
