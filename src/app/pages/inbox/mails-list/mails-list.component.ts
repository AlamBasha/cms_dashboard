import {
  Component,
  Input,
  Output,
  EventEmitter,
  OnInit,
  OnChanges
} from "@angular/core";
import { Mail } from "../mail.model";

@Component({
  selector: "app-mails-list",
  templateUrl: "mail-list.component.html",
  styleUrls: ["mail-list.component.css"]
})
export class MailsListComponent implements OnInit, OnChanges {
  @Input() mails: Mail[];
  @Output() currentMail: EventEmitter<any> = new EventEmitter();
  currMail: Mail;
  isClicked: boolean = false;

  ngOnInit() {}

  ngOnChanges() {}

  readMail(mail) {
    this.currMail = mail;
    this.isClicked = true;
    for (let i = 0; i < this.mails.length; i++) {
      if (this.mails[i].id == mail.id) {
        this.mails[i].read = "true";
      }
    }
    this.currentMail.emit({ mail: mail, mails: this.mails });
  }
}
