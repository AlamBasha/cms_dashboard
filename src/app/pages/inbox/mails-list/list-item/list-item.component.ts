import { Component, Input, OnInit } from "@angular/core";
import { Mail } from "../../mail.model";

@Component({
  selector: "app-list-item",
  templateUrl: "list-item.component.html",
  styleUrls: ["list-item.component.css"]
})
export class ListItemComponent implements OnInit {
  @Input() mail: Mail;

  ngOnInit() {}
}
