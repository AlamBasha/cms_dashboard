import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../shared/shared.module";
import { InboxComponent } from "./inbox.component";
import { MailsListComponent } from "./mails-list/mails-list.component";
import { ListItemComponent } from "./mails-list/list-item/list-item.component";
import { MailListItemComponent } from "./mails-list/mail-list-item/mail-list-item.component";
import { MailDetailsComponent } from "./mail-details/mail-details.component";

const inboxRoutes: Routes = [
  {
    path: "",
    component: InboxComponent
  }
];

@NgModule({
  declarations: [
    InboxComponent,
    MailsListComponent,
    MailListItemComponent,
    ListItemComponent,
    MailDetailsComponent
  ],
  imports: [SharedModule, RouterModule.forChild(inboxRoutes)]
})
export class MailModule {}
