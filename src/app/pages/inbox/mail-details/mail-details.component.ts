import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnChanges
} from "@angular/core";
import { Mail } from "../mail.model";
import { fuseAnimations } from "../../../animations";
import { MailService } from "../../../services/mail.service";

@Component({
  selector: "app-mail-details",
  templateUrl: "mail-details.component.html",
  styleUrls: ["mail-details.component.css"],
  animations: fuseAnimations,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MailDetailsComponent implements OnChanges {
  @Input() mail: Mail;
  showDetails = false;
  askmail: Mail;

  // constructor(private mailService: MailService) {}

  ngOnChanges() {}

  // markAsRead() {
  //   if (this.mail && !this.mail.read) {
  //     this.askmail.markRead();
  //     console.log("mail", this.askmail.read);
  //   }
  // }
  // updateModel(data) {
  //   this.mail = !data ? null : new Mail({ ...data });
  // }

  // updateMail() {
  //   this.updateModel(this.mail);
  // }
}
