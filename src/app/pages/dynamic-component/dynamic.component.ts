import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dynamic',
  templateUrl: './dynamic.component.html',
  styleUrls: ['./dynamic.component.css']
})
export class DynamicComponent implements OnInit {
  fields: any;
  isVisible: boolean;
  constructor() {
  }
  ngOnInit() {
    this.isVisible = false;
    this.fields = [
      {
        'id': 'imageid',
        'name': 'imageName',
        'value': ''
      }
    ];
  }
  addNewOne() {
    this.fields.push({
      'id': 'imageid',
      'name': 'imageName',
      'value': ''
    });
  }
  removeOne(field) {
    this.fields.splice(field, 1);
  }
  printAll() {
    this.isVisible = !this.isVisible;
  }
  onKey(event) {
    const inputName = event.target.name;
    const inputValue = event.target.value;
    for (let i = 0; i < this.fields.length; i++) {
      if (i==inputName) {
        this.fields[i].value = inputValue;
      }
    }
  }
}
