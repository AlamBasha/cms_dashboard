import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router, ActivatedRoute } from "@angular/router";
import { EventService } from "../../services/event.service";

@Component({
  selector: "app-event-creation",
  templateUrl: "event-creation.component.html",
  styleUrls: ["event-creation.component.css"]
})
export class EventCreationComponent implements OnInit {
  eventCreationForm: FormGroup;
  isValid: boolean;
  private formSubmitAttempt: boolean;

  constructor(
    private eventService: EventService,
    private fb: FormBuilder,
    private router: Router
  ) {
    this.eventCreationForm = this.fb.group({
      eventName: ["", Validators.required],
      startDatePicker: ["", Validators.required],
      dueDatePicker: ["", Validators.required],
      description: ["", Validators.required],
      location: ["", Validators.required]
    });
  }

  ngOnInit() {}

  isFieldInvalid(field: string) {
    return (
      (!this.eventCreationForm.get(field).valid &&
        this.eventCreationForm.get(field).touched) ||
      (this.eventCreationForm.get(field).untouched && this.formSubmitAttempt)
    );
  }
  onEventCreationSubmit() {
    if (this.eventCreationForm.valid) {
      this.eventService
        .createAnEvent(
          this.eventCreationForm.get("eventName").value,
          this.eventCreationForm.get("description").value,
          this.eventCreationForm.get("location").value,
          this.eventCreationForm.get("startDatePicker").value,
          this.eventCreationForm.get("dueDatePicker").value
        )
        .subscribe(data => {});
    }
    this.formSubmitAttempt = true;
  }
}
