import {NgModule} from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {SharedModule} from '../../shared/shared.module';
import {FormsModule, ReactiveFormsModule } from '@angular/forms';
import {EventCreationComponent} from './event-creation.component';
import {OwlDateTimeModule, OwlNativeDateTimeModule} from 'ng-pick-datetime';
const eventCreationRoutes: Routes = [
  {
    path: '',
    component: EventCreationComponent
  }
];

@NgModule({
  declarations: [
    EventCreationComponent
  ],
  imports: [
    FormsModule,
    ReactiveFormsModule,OwlDateTimeModule, OwlNativeDateTimeModule,
    SharedModule,
    RouterModule.forChild(eventCreationRoutes)
  ],
})
export class EventCreationModule { }
