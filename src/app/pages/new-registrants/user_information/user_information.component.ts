import { Component, OnInit, Inject } from "@angular/core";
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { Subscription } from "rxjs/Subscription";
import { NewRegistrationService } from "../../../services/new-registration.service";
import { NewRegistrantService } from "../../../services/new-registrants.service";
export interface DialogData {
  res: string;
}
export interface ReasonDialogData {
  reason: string;
}
@Component({
  selector: "user_information",
  styleUrls: ["user_information.component.css"],
  templateUrl: "user_information.component.html"
})
export class UserInformationComponent implements OnInit {
  res: string;
  onProductChanged: Subscription;
  user: any;
  reason: string = "";

  constructor(
    private newRegistrationService: NewRegistrationService,
    private newRegistrantService: NewRegistrantService,
    public dialog: MatDialog
  ) {}

  openDialog(imgRf: string): void {
    console.log(imgRf);
    const dialogRef = this.dialog.open(UserDialogComponent, {
      width: "370px",
      height: "432px",
      data: { res: imgRf }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log("The dialog was closed");
      this.res = result;
    });
  }
  openReasonDialog(): void {
    const dialogRf = this.dialog.open(ReasonDialogComponent, {
      width: "320px",
      data: { res: this.reason }
    });

    dialogRf.afterClosed().subscribe(result => {
      this.reason = result;
      this.newRegistrantService
        .rejectRegistration(this.user.primary_phone_number, this.reason)
        .subscribe(data => {
          console.log("app-res", data);
        });
    });
  }
  onButtonClick() {
    this.newRegistrantService
      .approveRegistration(this.user.primary_phone_number)
      .subscribe(data => {
        console.log("app-res", data);
      });
  }

  ngOnInit() {
    // Subscribe to update product on changes
    this.onProductChanged = this.newRegistrationService.onProductChanged.subscribe(
      user => {
        this.user = user;
        console.log("newreg", JSON.stringify(user));
      }
    );
  }

  ngOnDestroy() {
    this.onProductChanged.unsubscribe();
  }
}

@Component({
  selector: "user-dialog",
  styleUrls: ["user-dialog.css"],
  templateUrl: "user-dialog.html"
})
export class UserDialogComponent {
  constructor(
    public dialogRef: MatDialogRef<UserDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) {}

  onNoClick(): void {
    this.dialogRef.close();
  }
}

@Component({
  selector: "reject-reason-dialog",
  templateUrl: "reject-reason-dialog.html"
})
export class ReasonDialogComponent {
  constructor(
    public dialogRf: MatDialogRef<UserDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: ReasonDialogData
  ) {}

  onNoClick(): void {
    this.dialogRf.close();
  }
}
