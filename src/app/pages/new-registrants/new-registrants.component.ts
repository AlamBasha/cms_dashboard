import { Component, OnInit, ViewChild } from "@angular/core";
import { MatSort, MatTableDataSource, MatPaginator } from "@angular/material";
import { NewRegistrantService } from "../../services/new-registrants.service";

@Component({
  selector: "new-registrants-table",
  styleUrls: ["new-registrants.component.css"],
  templateUrl: "new-registrants.component.html"
})
export class NewRegistrantComponent implements OnInit {
  displayedColumns: string[] = [
    "no",
    "name",
    "primary_phone_number",
    "email_id",
    "view"
  ];
  dataSource = new MatTableDataSource();

  constructor(private registrantService: NewRegistrantService) {}

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  ngOnInit() {
    this.registrantService.getData().subscribe(data => {
      console.log("new data", data);
      this.dataSource.data = data;
    });

    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}
