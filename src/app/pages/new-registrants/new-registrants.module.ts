import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../shared/shared.module";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MatTableModule } from "@angular/material";
import { NewRegistrantComponent } from "./new-registrants.component";
import {
  UserInformationComponent,
  UserDialogComponent,
  ReasonDialogComponent
} from "./user_information/user_information.component";
import { NewRegistrationService } from "../../services/new-registration.service";

const appointmentsTableRoutes: Routes = [
  {
    path: "",
    component: NewRegistrantComponent
  },
  {
    path: "userdetails/:id",
    component: UserInformationComponent,
    resolve: {
      data: NewRegistrationService
    }
  }
];
@NgModule({
  declarations: [
    NewRegistrantComponent,
    UserInformationComponent,
    UserDialogComponent,
    ReasonDialogComponent
  ],
  entryComponents: [UserDialogComponent, ReasonDialogComponent],
  imports: [
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    MatTableModule,
    RouterModule.forChild(appointmentsTableRoutes)
  ]
})
export class NewRegistrantsModule {}
