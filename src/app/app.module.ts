import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { NgModule } from "@angular/core";
import {
  MatButtonModule,
  MatIconModule,
  MatMenuModule,
  MatSidenavModule,
  MatToolbarModule,
  MatListModule
} from "@angular/material";
import { SharedModule } from "./shared/shared.module";
import { AppComponent } from "./app.component";
import { FusePerfectScrollbarDirective } from "./shared/fuse-perfect-scrollbar/fuse-perfect-scrollbar.directive";
import { AuthGuard } from "./guards/auth.guard";
import { AuthService } from "./services/auth.service";
import { EventService } from "./services/event.service";
import { AuthComponent } from "./layouts/auth/auth.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { DashboardComponent } from "./layouts/dashboard/dashboard.component";
import { AuthModule } from "./pages/auth/auth.module";
import { HttpClientModule } from "@angular/common/http";
import { DashboardModule } from "./pages/dashboard/dashboard.module";
import { AppRoutingModule } from "./app.routing.module";
import { EventCreationModule } from "./pages/event-creation/event-creation.module";
import { AppointmentService } from "./services/appointments.service";
import { NewRegistrantService } from "./services/new-registrants.service";
import { BrandingRequestService } from "./services/branding-request.service";
import { DynamicComponent } from "./pages/dynamic-component/dynamic.component";
import { EarningsModule } from "./pages/earnings/earnings.module";
import { MailModule } from "./pages/inbox/mail.module";
import { MailService } from "./services/mail.service";
import { PropertyService } from "./services/property.service";
import { CityService } from "./services/city.service";
import { ManageLeadsService } from "./services/manage-leads.service";
import { ManageLeadsModule } from "./pages/manage-leads/manage-leads.module";
import { WidgetService } from "./services/widget.service";
import { LoaderComponent } from "./core/loader/loader.component";
import { LoaderService } from "./core/loader/loader.service";
import { NewRegistrationService } from "./services/new-registration.service";
@NgModule({
  declarations: [
    AppComponent,
    AuthComponent,
    DashboardComponent,
    FusePerfectScrollbarDirective,
    DynamicComponent,
    LoaderComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    SharedModule,
    MatMenuModule,
    MatSidenavModule,
    MatButtonModule,
    MatIconModule,
    MatListModule,
    MatToolbarModule,
    AuthModule,
    DashboardModule,
    EventCreationModule,
    EarningsModule,
    MailModule,
    ManageLeadsModule,
    AppRoutingModule
  ],
  providers: [
    AuthGuard,
    AuthService,
    PropertyService,
    EventService,
    AppointmentService,
    NewRegistrantService,
    MailService,
    BrandingRequestService,
    CityService,
    ManageLeadsService,
    WidgetService,
    LoaderService,
    NewRegistrationService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
