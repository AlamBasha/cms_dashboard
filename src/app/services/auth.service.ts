import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { HttpClient } from "@angular/common/http";
import { map } from "rxjs/operators";
import { LoaderService } from "../core/loader/loader.service";
@Injectable()
export class AuthService {
  constructor(
    private router: Router,
    private http: HttpClient,
    private loaderService: LoaderService
  ) {}
  login(phone: number, password: string) {
    this.loaderService.show();
    const data = { primary_phone_number: phone, password: password };
    return this.http
      .post<any>("https://www.prodx.in/cp/users/login", data)
      .pipe(
        map(res => {
          if (res["valid_auth"] === true) {
            localStorage.setItem("currentUser", res["valid_auth"]);
          }
          this.loaderService.hide();
          return res;
        })
      );
  }
  logout() {
    localStorage.removeItem("currentUser");
    this.router.navigate([""]);
  }
}
