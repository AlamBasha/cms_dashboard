import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Router } from "@angular/router";
import { map } from "rxjs/operators";
import { DataB } from "../models/manage-leads-data";
import { LoaderService } from "../core/loader/loader.service";
@Injectable()
export class ManageLeadsService {
  constructor(
    private router: Router,
    private http: HttpClient,
    private loaderService: LoaderService
  ) {}

  getData() {
    this.loaderService.show();
    // let data:DataB[];
    // return this.http.post<any>('https://www.prodx.in/cp/users/performance',data).pipe(map(res=>{
    // console.log(res);
    let data = [
      {
        lead_name: "Alam",
        lead_mobile: "9876543210",
        lead_email: "abc@gmail.com",
        cp_name: "cp",
        cp_mobile: "1234567890"
      },
      {
        lead_name: "Alam",
        lead_mobile: "9876543210",
        lead_email: "abc@gmail.com",
        cp_name: "cp",
        cp_mobile: "1234567890"
      },
      {
        lead_name: "Alam",
        lead_mobile: "9876543210",
        lead_email: "abc@gmail.com",
        cp_name: "cp",
        cp_mobile: "1234567890"
      }
    ];
    this.loaderService.hide();
    return data;
  }
}
