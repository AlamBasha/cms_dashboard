import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { HttpClient } from "@angular/common/http";
import { map } from "rxjs/operators";
import { Observable, of } from "rxjs";
import { LoaderService } from "../core/loader/loader.service";
@Injectable()
export class NewRegistrantService {
  constructor(
    private router: Router,
    private loaderService: LoaderService,
    private http: HttpClient
  ) {}

  public getData(): Observable<any> {
    this.loaderService.show();
    let data: any = [];
    return this.http
      .get<any>("https://www.prodx.in/cp/admin/view-all-reg-request")
      .pipe(
        map(res => {
          this.loaderService.hide();
          return res;
        })
      );
  }
  approveRegistration(phone: number) {
    this.loaderService.show();
    const data = { primary_phone_number: phone };
    return this.http
      .post<any>("https://www.prodx.in/cp/admin/approve-registration", data)
      .pipe(
        map(res => {
          console.log("app-res", res);
          this.loaderService.hide();
          return res;
        })
      );
  }
  rejectRegistration(phone: number, reason: string) {
    this.loaderService.show();
    const data = { primary_phone_number: phone, reason: reason };
    return this.http
      .post<any>("https://www.prodx.in/cp/admin/reject-registration", data)
      .pipe(
        map(res => {
          this.loaderService.hide();
          return res;
        })
      );
  }
}
