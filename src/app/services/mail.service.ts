import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { HttpClient } from "@angular/common/http";
import { map } from "rxjs/operators";
import { LoaderService } from "../core/loader/loader.service";
@Injectable()
export class MailService {
  constructor(
    private router: Router,
    private loaderService: LoaderService,
    private http: HttpClient
  ) {}
  getMails() {
    this.loaderService.show();
    this.loaderService.hide();
    return [
      {
        id: "1",
        cp_name: "basha",
        cp_mobile: "9182137475",
        subject: "Hello",
        message: "hii buddies",
        time: "2018-08-01",
        read: "true"
      },
      {
        id: "2",
        cp_name: "basha",
        cp_mobile: "9182137475",
        subject:
          "Commits that need to be pushed lorem ipsum dolor sit amet, consectetur adipiscing elit.",
        message:
          "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce lorem diam, pulvinar id nisl non, ultrices maximus nibh. Suspendisse ut justo velit. Nullam ac ultrices risus, quis auctor orci. Vestibulum volutpat nisi et neque porta ullamcorper. Maecenas porttitor porta erat ac suscipit. Sed cursus leo ut elementum fringilla. Maecenas semper viverra erat, vel ullamcorper dui efficitur in. Vestibulum placerat imperdiet tellus, et tincidunt eros posuere eget. Proin sit amet facilisis libero. Nulla eget est ut erat aliquet rhoncus. Quisque ac urna vitae dui hendrerit sollicitudin vel id sem.\n\n\n" +
          "\n" +
          "In eget ante sapien. Quisque consequat velit non ante finibus, vel placerat erat ultricies. Aliquam bibendum justo erat, ultrices vehicula dolor elementum a. Mauris eu nisl feugiat ligula molestie eleifend. Aliquam efficitur venenatis velit ac porta. Vivamus vitae pulvinar tellus. Donec odio enim, auctor eget nibh mattis, ultricies dignissim lacus. Phasellus non tincidunt dui. Nulla eu arcu lorem.",
        time: "2018-08-01",
        read: "false"
      },
      {
        id: "3",
        cp_name: "basha",
        cp_mobile: "9182137475",
        subject:
          "Commits that need to be pushed lorem ipsum dolor sit amet, consectetur adipiscing elit.",
        message: "hii buddies",
        time: "2018-08-01",
        read: "false"
      },
      {
        id: "4",
        cp_name: "basha",
        cp_mobile: "9182137475",
        subject:
          "Commits that need to be pushed lorem ipsum dolor sit amet, consectetur adipiscing elit.",
        message: "hii buddies",
        time: "2018-08-01",
        read: "false"
      },
      {
        id: "5",
        cp_name: "basha",
        cp_mobile: "9182137475",
        subject:
          "Commits that need to be pushed lorem ipsum dolor sit amet, consectetur adipiscing elit.",
        message: "hii buddies",
        time: "2018-08-01",
        read: "false"
      },
      {
        id: "6",
        cp_name: "basha",
        cp_mobile: "9182137475",
        subject:
          "Commits that need to be pushed lorem ipsum dolor sit amet, consectetur adipiscing elit.",
        message: "hii buddies",
        time: "2018-08-01",
        read: "true"
      },
      {
        id: "7",
        cp_name: "basha",
        cp_mobile: "9182137475",
        subject: "Hello",
        message: "hii buddies",
        time: "2018-08-01",
        read: "false"
      },
      {
        id: "8",
        cp_name: "basha",
        cp_mobile: "9182137475",
        subject: "Hello",
        message: "hii buddies",
        time: "2018-08-01",
        read: "true"
      },
      {
        id: "9",
        cp_name: "basha",
        cp_mobile: "9182137475",
        subject: "Hello",
        message: "hii buddies",
        time: "2018-08-01",
        read: "false"
      }
    ];
  }
}
