import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { HttpClient } from "@angular/common/http";
import { map } from "rxjs/operators";
import { Observable, of } from "rxjs";
import { LoaderService } from "../core/loader/loader.service";
@Injectable()
export class PropertyService {
  constructor(
    private router: Router,
    private http: HttpClient,
    private loaderService: LoaderService
  ) {}
  public getData(city): Observable<any> {
    this.loaderService.show();
    let data = city;
    return this.http
      .post<any>("https://www.prodx.in/cp/tvh_property", data)
      .pipe(
        map(res => {
          this.loaderService.hide();
          return res;
        })
      );
  }
}
