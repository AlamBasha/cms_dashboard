import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Router } from "@angular/router";
import { map } from "rxjs/operators";
import { DataB } from "../models/branding-request-data";
import { LoaderService } from "../core/loader/loader.service";
@Injectable()
export class BrandingRequestService {
  constructor(
    private router: Router,
    private http: HttpClient,
    private loaderService: LoaderService
  ) {}

  getData(sel) {
    this.loaderService.show();
    let data;
    if (sel == "All") {
      data = {};
    } else {
      data = { zonal_info: sel };
    }
    return this.http
      .post<any>("https://www.prodx.in/cp/users/performance", data)
      .pipe(
        map(res => {
          this.loaderService.hide();
          return res;
        })
      );
  }
}
