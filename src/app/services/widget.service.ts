import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { HttpClient } from "@angular/common/http";
import { map } from "rxjs/operators";
import { Observable, of } from "rxjs";

@Injectable()
export class WidgetService {
  constructor(private router: Router, private http: HttpClient) {}
  public getData(): Observable<any> {
    return this.http.get<any>("https://api.openweathermap.org/data/2.5/weather?q=mumbai,india &APPID=541125b0437d27d8e4ee146caeb67d3a").pipe(
      map(res => {
          //console.log("weather data"+ res);
        return res;
      })
    );
  }
}