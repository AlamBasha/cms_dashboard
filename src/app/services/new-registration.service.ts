import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import {
  ActivatedRouteSnapshot,
  Resolve,
  RouterStateSnapshot
} from "@angular/router";

import { Observable } from "rxjs/Observable";
import { BehaviorSubject } from "rxjs/BehaviorSubject";

@Injectable()
export class NewRegistrationService implements Resolve<any> {
  routeParams: any;
  user: any;
  onProductChanged: BehaviorSubject<any> = new BehaviorSubject({});

  constructor(private http: HttpClient) {}

  /**
   * Resolve
   * @param {ActivatedRouteSnapshot} route
   * @param {RouterStateSnapshot} state
   * @returns {Observable<any> | Promise<any> | any}
   */
  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<any> | Promise<any> | any {
    this.routeParams = route.params;

    return new Promise((resolve, reject) => {
      Promise.all([this.getDetails()]).then(() => {
        resolve();
      }, reject);
    });
  }

  getDetails(): Promise<any> {
    return new Promise((resolve, reject) => {
      let data = { primary_phone_number: this.routeParams.id };
      this.http
        .post("https://www.prodx.in/cp/admin/view-cp-reg-request", data)
        .subscribe((response: any) => {
          this.user = response;
          this.onProductChanged.next(this.user);
          resolve(response);
        }, reject);
    });
  }
}
