import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { HttpClient } from "@angular/common/http";
import { map } from "rxjs/operators";
import { LoaderService } from "../core/loader/loader.service";
@Injectable()
export class EventService {
  constructor(
    private router: Router,
    private loaderService: LoaderService,
    private http: HttpClient
  ) {}
  createAnEvent(
    eventName: string,
    description: string,
    location: string,
    startDate: string,
    endDate: string
  ) {
    this.loaderService.show();
    let sDate = new Date(startDate);
    let lDate = new Date(endDate);
    const data = {
      event_name: eventName,
      event_description: description,
      location: location,
      start_time: sDate.getTime(),
      end_time: lDate.getTime(),
      image_url: ""
    };
    return this.http
      .post<any>("https://www.prodx.in/cp/admin/add-events", data)
      .pipe(
        map(res => {
          this.loaderService.hide();
          return res;
        })
      );
  }
}
