import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { HttpClient } from "@angular/common/http";
import { map } from "rxjs/operators";
import { Observable, of } from "rxjs";
import { LoaderService } from "../core/loader/loader.service";
@Injectable()
export class CityService {
  constructor(
    private router: Router,
    private http: HttpClient,
    private loaderService: LoaderService
  ) {}
  public getCities(): Observable<any> {
    this.loaderService.show();
    return this.http.get<any>("https://www.prodx.in/cp/get_city").pipe(
      map(res => {
        this.loaderService.hide();
        return res;
      })
    );
  }
}
