export class DataB{
    id: number;
    selling_price: number;
    unit_count: number;
    zonal_info: string;
    timestamp: number;
  }