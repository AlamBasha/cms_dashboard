export class Data {
    name: string;
    position: number;
    weight: number;
    symbol: string;
  }
