import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import {AuthService} from '../../services/auth.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent {
  opened: boolean;
  mode: string;
  activeMenuItem: string;
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );
  constructor(
    private router: Router,
    private authService: AuthService,
    private breakpointObserver: BreakpointObserver
  ) {
    this.opened = true;
    this.mode = 'side';
    this.activeMenuItem = 'Dashboard';
    this.navigateTo(this.activeMenuItem);
  }

  navigateTo(page) {
    this.activeMenuItem = page;
    switch (page) {
      case 'Dashboard':
        this.router.navigate(['dashboard']);
        break;
      case 'New Registrations':
        this.router.navigate(['newRegistrants']);
        break;
      case 'Create an Event':
        this.router.navigate(['newEvent']);
        break;
      case 'Earnings':
        this.router.navigate(['earnings']);
        break;
      case 'Inbox':
        this.router.navigate(['inbox']);
        break;
      case 'Appointments':
        this.router.navigate(['newAppointments']);
        break;
      case 'Branding Request':
        this.router.navigate(['branding-request']);
        break;
      case 'Properties':
        this.router.navigate(['properties']);
        break;
      case 'ManageLeads':
        this.router.navigate(['manageleads']);
        break;
      case 'Top-Performance':
        this.router.navigate(['top-performance']);
        break;
    }
  }

  goToLogout() {
    this.authService.logout(); }

}
