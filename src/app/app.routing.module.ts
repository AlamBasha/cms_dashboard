import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {AuthGuard} from './guards/auth.guard';
import {AuthComponent} from './layouts/auth/auth.component';
import {DashboardComponent} from './layouts/dashboard/dashboard.component';
import {DynamicComponent} from './pages/dynamic-component/dynamic.component';

const appRoutes: Routes = [
  {
    path: '',
    component: AuthComponent,
    children: [
      {
        path: '',
        redirectTo: 'auth/login',
        pathMatch: 'full'
      },
      {
        path: 'auth',
        loadChildren: './pages/auth/auth.module#AuthModule'
      }
    ]
  },
  {
    path: '',
    component: DashboardComponent,
    children: [
      {
        path: 'dashboard',
        canActivate: [AuthGuard],
        loadChildren: './pages/dashboard/dashboard.module#DashboardModule'
      },
      {
        path: 'newEvent',
        canActivate: [AuthGuard],
        loadChildren: './pages/event-creation/event-creation.module#EventCreationModule'
      },
      {
        path: 'newAppointments',
        canActivate: [AuthGuard],
        loadChildren: './pages/appointments/appointments.module#AppointmentsModule'
      },
      {
        path: 'newRegistrants',
        canActivate: [AuthGuard],
        loadChildren: './pages/new-registrants/new-registrants.module#NewRegistrantsModule'
      },
      {
        path: 'earnings',
        canActivate: [AuthGuard],
        loadChildren: './pages/earnings/earnings.module#EarningsModule'
      },
      {
        path: 'properties',
        canActivate: [AuthGuard],
        loadChildren: './pages/properties/tabs.module#TabsModule'
      },
      {
        path: 'branding-request',
          canActivate: [AuthGuard],
          loadChildren: './pages/branding-request/branding-request.module#BrandingRequestModule'
      },
      {
        path: 'inbox',
        canActivate: [AuthGuard],
        loadChildren: './pages/inbox/mail.module#MailModule'
      },
      {
        path:'manageleads',
        canActivate:[AuthGuard],
        loadChildren:'./pages/manage-leads/manage-leads.module#ManageLeadsModule'
      },
      {
        path:'top-performance',
        canActivate:[AuthGuard],
        loadChildren:'./pages/top-performances/top-performance.module#TopPerformanceModule'
      }
    ]
  },
  {
    path: 'dynamic',
    component: DynamicComponent,
  }
];
@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
